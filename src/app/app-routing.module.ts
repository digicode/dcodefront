import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DriversComponent} from './Components/drivers/drivers.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {TestBComponent} from './Components/drivers/test-b/test-b.component';
import {InteretsComponent} from './Components/interets/interets.component';
import {ThanksComponent} from './Components/thanks/thanks.component';
import {HomeMailComponent} from './Components/home-mail/home-mail.component';

const routes: Routes = [
  {path: '', component: HomeMailComponent},
  {path: 'drivers', component: DriversComponent},
  {path: 'drivers/testB', component: TestBComponent},
  {path: 'interets/choice', component: InteretsComponent},
  {path: 'thanks', component: ThanksComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
