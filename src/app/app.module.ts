import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ApiService} from './Services/api.service';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeMailComponent} from './Components/home-mail/home-mail.component';
import {DriversComponent} from './Components/drivers/drivers.component';
import {TestAComponent} from './Components/drivers/test-a/test-a.component';
import {MobileAComponent} from './Components/drivers/test-a/mobile-a/mobile-a.component';
import {DesktopAComponent} from './Components/drivers/test-a/desktop-a/desktop-a.component';
import {TestBComponent} from './Components/drivers/test-b/test-b.component';
import {InteretsComponent} from './Components/interets/interets.component';
import {ThanksComponent} from './Components/thanks/thanks.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {Ng4LoadingSpinnerModule} from 'ng4-loading-spinner';


@NgModule({
  declarations: [
    AppComponent,
    HomeMailComponent,
    DriversComponent,
    TestAComponent,
    MobileAComponent,
    DesktopAComponent,
    TestBComponent,
    InteretsComponent,
    ThanksComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot(),
  ],
  providers: [
    ApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
