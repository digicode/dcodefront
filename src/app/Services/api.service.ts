import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {Observable} from 'rxjs';
import {Question} from '../Components/drivers/test-a/models/question';
import {Interest} from '../Components/interets/models/interest';
import {Answer} from '../Components/drivers/test-a/models/answer';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private _urlDriver = 'http://127.0.0.1:5000/questions';

  get urlDriver(): string {
    return this._urlDriver;
  }

  set urlDriver(value: string) {
    this._urlDriver = value;
  }

  private _urlInterets = 'http://127.0.0.1:5000/themes';

  get urlInterets(): string {
    return this._urlInterets;
  }

  set urlInterets(value: string) {
    this._urlInterets = value;
  }

  private _urlAnswers = 'http://127.0.0.1:5000/answers';


  get urlAnswers(): string {
    return this._urlAnswers;
  }

  set urlAnswers(value: string) {
    this._urlAnswers = value;
  }

  private _urlUsersInterets = 'http://127.0.0.1:5000/users/1/themes';


  get urlUsersInterets(): string {
    return this._urlUsersInterets;
  }

  set urlUsersInterets(value: string) {
    this._urlUsersInterets = value;
  }

  private _urlUsersAnswers = 'http://127.0.0.1:5000/users/1/answers';


  get urlUsersAnswers(): string {
    return this._urlUsersAnswers;
  }

  set urlUsersAnswers(value: string) {
    this._urlUsersAnswers = value;
  }

  constructor(private httpClient: HttpClient) {
  }

  // TODO : Mettre a jour les Requetes!

  // Recup des themes/Interest depuis la DB
  getInteretsFromAPI(): Observable<Interest[]> {
    return this.httpClient.get<Interest[]>(`${this.urlInterets}`);
  }

  // Recup des Réponses depuis la DB
  getAnswersDriversFromAPI(): Observable<Answer[]> {
    return this.httpClient.get<Answer[]>(`${this.urlAnswers}`);
  }

  // Recup des Question depuis la DB
  getQuestionFromAPI(questionId: number): Observable<Question> {
    return this.httpClient.get<Question>(`${this.urlDriver}` + '/' + questionId);
  }

  // Recup des Old data depuis la DB
  getAnswersWithOldDataUserFromAPI(UserID): Observable<Answer[]> {
    return this.httpClient.get<Answer[]>(`${this.urlUsersAnswers}`);
  }

  // getOldInteretByID(id): Observable<Interest[]> {
  //   return this.httpClient.get<Interest[]>(`${this.urlUsersInterets}/${id}`);
  // }
  //

  // POST des réponses testB en DB
  addAnswers(answerId: number, questionId: number): Observable<string> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('answerId', answerId.toString());
    httpParams = httpParams.append('questionId', questionId.toString());
    return this.httpClient.post<string>(`${this.urlUsersAnswers}`, httpParams);
  }

  // POST des Interest en DB

  addInterets(themeId: number): Observable<string> {
    return this.httpClient.post<string>(this.urlUsersInterets + '/' + themeId, null);
  }

  //
  // UpdateAnswerFromUser(  id, data: ): Observable<> {
  //   return this.httpClient.put<>(`${this.url}/${id}`, data);
  // }
}





