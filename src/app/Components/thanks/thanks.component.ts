import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.css']
})
export class ThanksComponent implements OnInit {

  private _thanks: string;

  get thanks(): string {
    return this._thanks;
  }

  set thanks(value: string) {
    this._thanks = value;
  }

  constructor() {
    this.thanks = 'test achats vous remercie pour vos réponses';
  }

  ngOnInit() {
  }

}
