import {Component, OnInit} from '@angular/core';
import {Interest} from './models/interest';
import {ApiService} from '../../Services/api.service';
import {InterestService} from './services/interest.service';
import {ThemeService} from './services/theme.service';
import {map, switchMap} from 'rxjs/operators';
import {Theme} from './models/theme';


@Component({
  selector: 'app-interets',
  templateUrl: './interets.component.html',
  styleUrls: ['./interets.component.css']
})
export class InteretsComponent implements OnInit {

  private _userId: number;
  private _interestId: number;
  private _interest: Interest;
  private _question: string;
  private _nbChecked: number;
  private _noChoice: boolean;
  private _mPage: number;

  get userId(): number {
    return this._userId;
  }

  set userId(value: number) {
    this._userId = value;
  }

  get interestId(): number {
    return this._interestId;
  }

  set interestId(value: number) {
    this._interestId = value;
  }

  get interest(): Interest {
    return this._interest;
  }

  set interest(value: Interest) {
    this._interest = value;
  }

  get question(): string {
    return this._question;
  }

  set question(value: string) {
    this._question = value;
  }

  get nbChecked(): number {
    return this._nbChecked;
  }

  set nbChecked(value: number) {
    this._nbChecked = value;
  }

  get noChoice(): boolean {
    return this._noChoice;
  }

  set noChoice(value: boolean) {
    this._noChoice = value;
  }

  get mPage(): number {
    return this._mPage;
  }

  set mPage(value: number) {
    this._mPage = value;
  }

  public interetMobile;

  constructor(private apiService: ApiService, private interestService: InterestService, private themeService: ThemeService) {
  }

  ngOnInit() {
    this.userId = 1;
    this.mPage = 0;
    this.interestId = 1;
    this.interestService.getInterest(this.interestId).pipe(map((interest: Interest) => {
      this.interest = interest;
    })).pipe(switchMap(() => this.themeService.getUserInterestThemes(this.userId, this.interest.id))
    ).subscribe((userThemes: Theme[]) => {
      this.nbChecked = 0;
      this.noChoice = true;
      userThemes.forEach(userTheme => {
        if (this.interest.themes.map(theme => theme.id).includes(userTheme.id)) {
          const key = this.interest.themes.indexOf(this.interest.themes.find(theme => {
            return (theme.id === userTheme.id);
          }));
          this.interest.themes[key].interested = userTheme.interested;
          if (this.interest.themes[key].interested) {
            this.nbChecked++;
            this.noChoice = false;
          }
        }
      });
    });
  }

  onCheckBoxChange(theme, event) {
    const themeId = theme.id;
    const interested = event.target.checked;
    this.themeService.postUserTheme(this.userId, themeId, interested).subscribe(added => {
      this.nbChecked = interested ? this.nbChecked + 1 : this.nbChecked - 1;
      if (this.noChoice) {
        this.noChoice = false;
        this.nbChecked--;
      }
      // @ts-ignore
      document.getElementById('checkboxnone').checked = false;
    });
  }

  onZeroChoice(choicesTable, event) {
    if (event.target.checked) {
      this.themeService.postUserThemes(this.userId, this.interest.id, false).subscribe(added => {
        document.querySelectorAll('input.checkoption').forEach(input => {
          // @ts-ignore
          input.checked = false;
        });
        this.nbChecked = 1;
        this.noChoice = true;
      });
    } else {
      this.nbChecked = 0;
      this.noChoice = false;
    }
  }

  onClickNext() {
    console.log('je passe NExt');
    this._mPage++;
    this.interetMobile = this.interest[this._mPage];
    console.log(this.interetMobile);
  }

  onClickPrevious() {
    console.log('je passe Previous');
    this._mPage--;
    this.interetMobile = this.interest[this._mPage];
  }
}
