import {Theme} from './theme';

export class Interest {
  constructor(private _id: number, private _title: string, private _themes: Theme[]) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get themes(): Theme[] {
    return this._themes;
  }

  set themes(value: Theme[]) {
    this._themes = value;
  }
}
