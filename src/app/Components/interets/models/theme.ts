export class Theme {
  constructor(private _id: number, private _content: string, private _interested: boolean) {
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get content(): string {
    return this._content;
  }

  set content(value: string) {
    this._content = value;
  }

  get interested(): boolean {
    return this._interested;
  }

  set interested(value: boolean) {
    this._interested = value;
  }
}
