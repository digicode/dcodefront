import {TestBed} from '@angular/core/testing';

import {ThemeService} from './theme.service';
import {HttpClientModule} from '@angular/common/http';

describe('ThemeService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
    ],
  }));

  it('should be created', () => {
    const service: ThemeService = TestBed.get(ThemeService);
    expect(service).toBeTruthy();
  });

  // First test of test
  // it('should return expected heroes (HttpClient called once)', () => {
  //   const expectedHeroes: Hero[] =
  //     [{ id: 1, name: 'A' }, { id: 2, name: 'B' }];
  //
  //   httpClientSpy.get.and.returnValue(asyncData(expectedHeroes));
  //
  //   heroService.getHeroes().subscribe(
  //     heroes => expect(heroes).toEqual(expectedHeroes, 'expected heroes'),
  //     fail
  //   );
  //   expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  // });
});
