import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Theme} from '../models/theme';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  constructor(private client: HttpClient) {
  }

  getUserInterestThemes(userId: number, interestId: number): Observable<Theme[]> {
    return this.client.get<Theme[]>('http://localhost:5000/users/' + userId + '/interests/' + interestId + '/themes');
  }

  postUserTheme(userId: number, themeId: any, interested: any): Observable<string> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('interested', interested);
    return this.client.post<string>('http://localhost:5000/users/' + userId + '/themes/' + themeId, httpParams);
  }

  postUserThemes(userId: number, interestId: number, interested: any) {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('interestId', interestId.toString());
    httpParams = httpParams.append('interested', interested);
    return this.client.post<string>('http://localhost:5000/users/' + userId + '/themes', httpParams);
  }
}
