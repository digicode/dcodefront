import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Interest} from '../models/interest';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InterestService {

  constructor(private client: HttpClient) {
  }

  getInterest(interestId: number): Observable<Interest> {
    return this.client.get<Interest>('http://localhost:5000/interests/' + interestId);
  }
}
