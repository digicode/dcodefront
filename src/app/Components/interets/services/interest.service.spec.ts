import {TestBed} from '@angular/core/testing';

import {InterestService} from './interest.service';
import {HttpClientModule} from '@angular/common/http';

describe('InterestService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
    ],
  }));

  it('should be created', () => {
    const service: InterestService = TestBed.get(InterestService);
    expect(service).toBeTruthy();
  });
});
