import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMailComponent } from './home-mail.component';

describe('HomeMailComponent', () => {
  let component: HomeMailComponent;
  let fixture: ComponentFixture<HomeMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
