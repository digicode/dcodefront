import { Component, OnInit } from '@angular/core';
import {map, switchMap} from 'rxjs/operators';
import {Question} from './test-a/models/question';
import {Answer} from './test-a/models/answer';
import {QuestionService} from './test-a/services/question.service';
import {AnswerService} from './test-a/services/answer.service';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.css']
})
export class DriversComponent implements OnInit {

  private _testing: string;
  private userId: number;
  private questionId: number;
  private question: Question;
  get testing(): string {
    return this._testing;
  }

  set testing(value: string) {
    this._testing = value;
  }

  constructor(
    private questionService: QuestionService,
    private answerService: AnswerService,
    private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show();
    this.testing = 'A';
    this.userId = 1;
    this.questionId = 1;

    this.questionService.getQuestion(this.questionId).pipe(map((question: Question) => {
      this.question = question;
    })).pipe(switchMap(() => this.answerService.getUserQuestionAnswers(this.userId, this.question.id))
    ).subscribe((userAnswers: Answer[]) => {
      this.spinnerService.hide();
      userAnswers.forEach(userAnswer => {
        if (this.question.answers.map(answer => answer.id).includes(userAnswer.id)) {
          const key = this.question.answers.indexOf(this.question.answers.find(answer => {
            return (answer.id === userAnswer.id);
          }));
          this.question.answers[key].note = userAnswer.note;
        }
      });
    });
  }

  testChanged(event) {
    this.testing = event.target.id;
  }
}
