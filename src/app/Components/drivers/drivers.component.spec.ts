import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DriversComponent} from './drivers.component';
import {TestAComponent} from './test-a/test-a.component';
import {TestBComponent} from './test-b/test-b.component';
import {DesktopAComponent} from './test-a/desktop-a/desktop-a.component';
import {MobileAComponent} from './test-a/mobile-a/mobile-a.component';
import {Ng4LoadingSpinnerComponent, Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {HttpClientModule} from '@angular/common/http';

describe('DriversComponent', () => {
  let component: DriversComponent;
  let fixture: ComponentFixture<DriversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DriversComponent,
        TestAComponent,
        DesktopAComponent,
        MobileAComponent,
        TestBComponent,
        Ng4LoadingSpinnerComponent
      ],
      providers: [
        Ng4LoadingSpinnerService,
      ],
      imports: [
        HttpClientModule,
      ]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
