import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Question} from '../models/question';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private client: HttpClient) {
  }

  getQuestion(questionId: number): Observable<Question> {
    return this.client.get<Question>('http://localhost:5000/questions/' + questionId);
  }
}
