import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Answer} from '../models/answer';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {

  constructor(private client: HttpClient) {
  }

  getUserQuestionAnswers(userId: number, questionId: number): Observable<Answer[]> {
    return this.client.get<Answer[]>('http://localhost:5000/users/' + userId + '/questions/' + questionId + '/answers');
  }

  postUserAnswer(userId: number, answerId: number, note: any): Observable<string> {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('note', note);
    return this.client.post<string>('http://localhost:5000/users/' + userId + '/answers/' + answerId, httpParams);
  }
}
