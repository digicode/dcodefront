import { TestBed } from '@angular/core/testing';

import { AnswerService } from './answer.service';
import {HttpClientModule} from '@angular/common/http';

describe('AnswerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule,
    ],
  }));

  it('should be created', () => {
    const service: AnswerService = TestBed.get(AnswerService);
    expect(service).toBeTruthy();
  });
});
