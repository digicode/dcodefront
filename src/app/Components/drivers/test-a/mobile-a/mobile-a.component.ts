import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Question} from '../models/question';
import {Answer} from '../models/answer';
import {AnswerService} from '../services/answer.service';

@Component({
  selector: 'app-mobile-a',
  templateUrl: './mobile-a.component.html',
  styleUrls: ['./mobile-a.component.css']
})
export class MobileAComponent implements OnInit, OnChanges {

  private _userId: number;
  private _question: Question;
  private _answers: Answer[];
  private _mPage: number;

  get userId(): number {
    return this._userId;
  }

  @Input() set userId(value: number) {
    this._userId = value;
  }

  get question(): Question {
    return this._question;
  }

  @Input() set question(value: Question) {
    this._question = value;
  }

  get answers(): Answer[] {
    return this._answers;
  }

  @Input() set answers(value: Answer[]) {
    this._answers = value;
  }

  get mPage(): number {
    return this._mPage;
  }

  set mPage(value: number) {
    this._mPage = value;
  }

  constructor(private answerService: AnswerService) { }

  ngOnInit() {
    this.mPage = 0;
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  previousPage() {
    this.mPage--;
  }

  nextPage() {
    this.mPage++;
  }

  questionAnswered(answer: Answer, event: any) {
    const answerId = answer.id;
    const note = event.target.value;
    this.answerService.postUserAnswer(this.userId, answerId, note).subscribe(added => {
      console.log(added);
      this.question.answers[this.mPage].note = note;
    });
  }
}
