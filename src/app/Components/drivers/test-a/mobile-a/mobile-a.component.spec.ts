import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MobileAComponent} from './mobile-a.component';
import {Ng4LoadingSpinnerComponent, Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {HttpClientModule} from '@angular/common/http';

describe('MobileAComponent', () => {
  let component: MobileAComponent;
  let fixture: ComponentFixture<MobileAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MobileAComponent,
        Ng4LoadingSpinnerComponent
      ],
      providers: [
        Ng4LoadingSpinnerService,
      ],
      imports: [
        HttpClientModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
