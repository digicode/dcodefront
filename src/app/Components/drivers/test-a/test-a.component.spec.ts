import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestAComponent} from './test-a.component';
import {DesktopAComponent} from './desktop-a/desktop-a.component';
import {MobileAComponent} from './mobile-a/mobile-a.component';
import {Ng4LoadingSpinnerComponent, Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {HttpClientModule} from '@angular/common/http';

describe('TestAComponent', () => {
  let component: TestAComponent;
  let fixture: ComponentFixture<TestAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestAComponent,
        DesktopAComponent,
        MobileAComponent,
        Ng4LoadingSpinnerComponent
      ],
      providers: [
        Ng4LoadingSpinnerService,
      ],
      imports: [
        HttpClientModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
