import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DesktopAComponent} from './desktop-a.component';
import {Ng4LoadingSpinnerComponent, Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {HttpClientModule} from '@angular/common/http';

describe('DesktopAComponent', () => {
  let component: DesktopAComponent;
  let fixture: ComponentFixture<DesktopAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DesktopAComponent,
        Ng4LoadingSpinnerComponent
      ],
      providers: [
        Ng4LoadingSpinnerService,
      ],
      imports: [
        HttpClientModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
