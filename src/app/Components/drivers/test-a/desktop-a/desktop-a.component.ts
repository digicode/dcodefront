import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../models/question';
import {Answer} from '../models/answer';
import {AnswerService} from '../services/answer.service';

@Component({
  selector: 'app-desktop-a',
  templateUrl: './desktop-a.component.html',
  styleUrls: ['./desktop-a.component.css']
})
export class DesktopAComponent implements OnInit {

  private _userId: number;
  private _question: Question;
  private _answers: Answer[];

  get userId(): number {
    return this._userId;
  }

  @Input() set userId(value: number) {
    this._userId = value;
  }

  get question(): Question {
    return this._question;
  }

  @Input() set question(value: Question) {
    this._question = value;
  }

  get answers(): Answer[] {
    return this._answers;
  }

  @Input() set answers(value: Answer[]) {
    this._answers = value;
  }

  constructor(private answerService: AnswerService) { }

  ngOnInit() {
  }

  questionAnswered(answer: Answer, event: any) {
    const answerId = answer.id;
    const note = event.target.value;
    this.answerService.postUserAnswer(this.userId, answerId, note).subscribe(added => {
      console.log(added);
    });
  }
}
