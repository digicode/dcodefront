import {Component, Input, OnInit} from '@angular/core';
import {Question} from './models/question';


@Component({
  selector: 'app-test-a',
  templateUrl: './test-a.component.html',
  styleUrls: ['./test-a.component.css']
})
export class TestAComponent implements OnInit {

  private _userId: number;
  private _question: Question;

  get userId(): number {
    return this._userId;
  }

  @Input() set userId(value: number) {
    this._userId = value;
  }

  get question(): Question {
    return this._question;
  }

  @Input() set question(value: Question) {
    this._question = value;
  }

  constructor() {
  }

  ngOnInit() {
  }
}
