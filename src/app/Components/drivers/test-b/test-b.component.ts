import {Component, Input, OnInit} from '@angular/core';
import {Question} from '../test-a/models/question';
import {ApiService} from '../../../Services/api.service';
import {Answer} from '../test-a/models/answer';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';


@Component({
  selector: 'app-test-b',
  templateUrl: './test-b.component.html',
  styleUrls: ['./test-b.component.css']
})
export class TestBComponent implements OnInit {

  private _userId: number;
  private _questionId: number;
  private _question: Question;
  private _answers: Answer[];
  private _mPage: number;
  private _theme: string;

  get userId(): number {
    return this._userId;
  }

  @Input() set userId(value: number) {
    this._userId = value;
  }

  get questionId(): number {
    return this._questionId;
  }

  set questionId(value: number) {
    this._questionId = value;
  }

  @Input() get question(): Question {
    return this._question;
  }

  set question(value: Question) {
    this._question = value;
  }

  get theme(): string {
    return this._theme;
  }

  set theme(value: string) {
    this._theme = value;
  }

  get answers(): Answer[] {
    return this._answers;
  }

  set answers(value: Answer[]) {
    this._answers = value;
  }

  get mPage(): number {
    return this._mPage;
  }

  set mPage(value: number) {
    this._mPage = value;
  }

  @Input()
  public answerId;
  @Input()
  public answerMobile;

  constructor(private apiService: ApiService, private spinnerService: Ng4LoadingSpinnerService) {
    this.theme = 'le theme';
  }

  onClickNext() {
    this.answerId++;
    this.answerMobile = this.question.answers[this.answerId];
    this.mPage++;

  }

  onClickPrevious() {
    this.answerId--;
    this.answerMobile = this.question.answers[this.answerId];
    this.mPage--;
  }

  onSelectionChange(element2) {
    this.apiService.addAnswers(element2.id, this.question.id).subscribe((data => {
    }));
  }

  ngOnInit() {
    this.mPage = 0;
  }
}
