import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TestBComponent} from './test-b.component';
import {Ng4LoadingSpinnerComponent, Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {HttpClientModule} from '@angular/common/http';

describe('TestBComponent', () => {
  let component: TestBComponent;
  let fixture: ComponentFixture<TestBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestBComponent,
        Ng4LoadingSpinnerComponent
      ],
      providers: [
        Ng4LoadingSpinnerService,
      ],
      imports: [
        HttpClientModule,
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
